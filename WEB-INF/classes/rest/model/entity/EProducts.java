package rest.model.entity;

import java.io.Serializable;

   
//JPA (Begin)
import javax.persistence.*;
//JPA (End)   
  

@Entity
@Table(name = "public.products")
public class EProducts implements Serializable
{
  @Id
  @Column(name = "id")
  private Integer productID;
  @Column(name = "name")
  private String productName;
  
  public Integer getStudentID() {
    return productID;
  }
  
  public void setProductID(Integer pID) {
    productID = pID;
  }  
  
  public String getProductName() {
    return productName;
  }
  
  public void setStudentName(String pName) {
    productName = pName;
  }
}
package rest;


public class Product
{
    private int id;
    private String name;
    private double cost;
    private String desc;
public Product(int id1, String name1,double cost1,String desc1){
      id = id1;
      name=name1;
      cost=cost1;
      desc=desc1;
 }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

     public double getCost() {
            return cost;
        }

        public void setName(double cost) {
            this.cost = cost;
        }
         public String getDesc() {
                return desc;
            }

            public void setDesc(String desc) {
                this.desc = desc;
            }
}